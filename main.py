# from guppy import hpy; hp=hpy()

import argparse
import ijson
import pathlib
import csv
import hashlib

parser = argparse.ArgumentParser(description='Process json.')
parser.add_argument('--source',
                    type=pathlib.Path,
                    required=True,
                    help='path to a local json file')
parser.add_argument('--dest',
                    type=pathlib.Path,
                    required=True,
                    help='path to a result file')
args = parser.parse_args()

md5_hash = hashlib.md5()


def get_column_hash(column_name):
    if len(column_name) > 32:
        md5_hash.update(column_name.encode('utf-8'))
        return md5_hash.hexdigest()
    else:
        return column_name


def convert_json_to_csv(json_path, csv_path):
    with open(json_path) as json_handler:
        acceptable_event = ('null', 'boolean', 'integer', 'double', 'number', 'string')

        id_column_hash = get_column_hash('id')
        columns_hash_uniq = set()
        columns_hash_sorted = [id_column_hash]
        columns_sorted = ['id']

        for prefix, event, _ in ijson.parse(json_handler):
            if event in acceptable_event:
                object_id, column_name = prefix.split('.', 1)
                column_hash = get_column_hash(column_name)
                if column_hash not in columns_hash_uniq:
                    columns_hash_uniq.add(column_hash)
                    columns_hash_sorted.append(column_hash)
                    columns_sorted.append(column_name)

        with open(csv_path, "w") as csv_handler:

            csv_writer = csv.writer(csv_handler)
            csv_writer.writerow(columns_sorted)
            buffer = {}

            del columns_hash_uniq
            del columns_sorted

            json_handler.seek(0)
            for prefix, event, value in ijson.parse(json_handler):
                if prefix != '':
                    prefix_parts = prefix.split('.', 1)
                    if event == 'start_map' and len(prefix_parts) == 1:
                        buffer[id_column_hash] = prefix_parts[0]
                    elif event in acceptable_event:
                        column_hash = get_column_hash(prefix_parts[1])
                        if buffer.get(column_hash):
                            buffer[column_hash] = f'{buffer[column_hash]},{value}'
                        else:
                            buffer[column_hash] = value
                    elif event == 'end_map' and len(prefix_parts) == 1:
                        row_data = [buffer.get(column_hash, '') for column_hash in columns_hash_sorted]
                        csv_writer.writerow(row_data)
                        csv_handler.flush()
                        buffer = {}


if __name__ == '__main__':
    convert_json_to_csv(args.source, args.dest)

# h = hp.heap()
# print(h)
